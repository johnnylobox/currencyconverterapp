const FormResources = {

    form_data: {
        ip_address: '',
        import: '100',
        iso_from: '',
        iso_to: '',
        rate_from: '',
        rate_to: '',
        convertion: '',
    },

    body_info: {
        show: false,
        import: '',
        iso_from: '',
        iso_to: '',
        rate_from: '',
        rate_to: '',
        import_convertion_str: '',
    },

}

export default FormResources;