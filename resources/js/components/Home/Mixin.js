import { Notyf } from 'notyf'
import 'notyf/notyf.min.css'
const notyf = new Notyf({duration: 8000})
import Resources from './Resources.js'
import FormResources from './FormResources.js'
import CurrencyAPI from '@everapi/currencyapi-js'

const mixin = {
    
    data(){
        return {
            notyf,
            locale_endpoint: '/currency-converter-store',
            currencies: Resources.currencies,
            form_data: FormResources.form_data,
            body_info: FormResources.body_info,
            ApiCurrencies: null,
            import_convertion_str: '',
            params: {
                base_currency: '',
                currencies: ''
            },
            results: null,
            currencyApi: new CurrencyAPI('cur_live_FBOtGRveJkWiSlE7g7fwif85btC4LIsYi05cPSGX'),
        }
    },

    watch: {

        async 'form_data.iso_from'(val)
        {
            if(val)
                await this.getQueryConverterAPI(val)
        },

    },

    methods:{

        importConvertion()
        {
            let imported = (parseFloat(this.form_data.import) * parseFloat(this.ApiCurrencies[this.form_data.iso_to].value))
            this.import_convertion_str = `${imported.toFixed(4)} ${this.ApiCurrencies[this.form_data.iso_to].code}`
        },

        async getQueryConverterAPI(acrom)
        {
            this.loader(true)
            this.ApiCurrencies = null
            await this.currencyApi.latest(acrom).then(response => {
                if(response.data)
                    this.ApiCurrencies = response.data
            })
            this.loader()
        },

        converseRevert()
        {
            let currency_from = this.form_data.iso_from
            let currency_to = this.form_data.iso_to

            this.form_data.iso_from = currency_to
            this.form_data.iso_to = currency_from
        },

        async SubmitConverter()
        {
            this.loader(true)

            this.form_data.rate_from = this.ApiCurrencies[this.form_data.iso_from].value.toFixed(4)
            this.form_data.rate_to = this.ApiCurrencies[this.form_data.iso_to].value.toFixed(4)
            this.form_data.convertion = (parseFloat(this.form_data.import) * parseFloat(this.ApiCurrencies[this.form_data.iso_to].value))

            let response = await this.post(this.locale_endpoint, this.form_data)

            if(response.status)
            {
                let item = response.data.item
                this.body_info.show = true
                this.body_info.import = item.import
                this.body_info.iso_from = item.iso_from
                this.body_info.iso_to = item.iso_to
                this.body_info.rate_from = item.rate_from
                this.body_info.rate_to = item.rate_to
                this.body_info.convertion = item.convertion

                let imported = (parseFloat(this.body_info.import) * parseFloat(this.body_info.rate_to))
                this.import_convertion_str = `${imported.toFixed(4)} ${this.body_info.iso_to}`
                notyf.success('Conversión exitosa')
            }
            else
            {
                this.errorFilter(response)
            }

            this.loader()
        },

    },
}

export default mixin