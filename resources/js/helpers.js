import { Notyf } from 'notyf'
import 'notyf/notyf.min.css'
const notyf = new Notyf({duration: 8000})

const connections = {

    methods: {

        async IpCallerApi()
        {
            const URL_API = "https://api.ipify.org/?format=json";
            let ip = null
            await fetch(URL_API)
                .then(responseRaw => responseRaw.json())
                .then(response => {
                    ip = response.ip
                }
            )
            return ip
        },

        loader(bool = false)
        {
            if(bool)
            {
                window.$('#loader').modal('show')
                return true
            }
            setTimeout( () => {
                window.$('#loader').modal('hide')
            }, 1000)
        },


        errorFilter(error)
        {
            if(typeof error.data === 'object')
                notyf.error(`${error.data.message}`)
            else
                notyf.error(error.data.message)
        },

    },

}

export default connections