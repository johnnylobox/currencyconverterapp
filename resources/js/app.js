import { createApp } from 'vue'
import connections from '@/js/connections'
import helpers from '@/js/helpers'
import HomeComponent from './components/Home/HomeComponent.vue'

const app = createApp({
    setup() {
        return {
            // message: 'Welcome to Your Vue.js App',
        };
    },
    components: {
        HomeComponent,
    },
})

app.mixin({
    mixins: [
        connections,
        helpers,
    ],
})

app.mount('#app')