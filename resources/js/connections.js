import axios from 'axios'

const axios_instance = axios.create({
    baseURL: '',
    headers: {
        'Content-Type': 'application/json',
    },
})

const connections = {

    methods: {
    
        async get(url)
        {
            return await axios_instance.get(url)
            .then(response => ({ 
                status: true, 
                number_status: response.status, 
                data: response.data 
            }))
            .catch(error => ({
                status: false,
                number_status: error.response ? error.response.status : error, 
                data: error.response ? error.response.data : error.message 
            }))
        },
    
        async post(url,data) 
        {
            return await axios_instance.post(url,data)
            .then(response => ({ 
                status: true,
                number_status: response.status,
                data: response.data,
            }))
            .catch(error => ({
                status: false,
                number_status: error.response ? error.response.status : error, 
                data: error.response ? error.response.data : error.message 
            }))
        },
    
        async put(url,data) 
        {
            return await axios_instance.put(url,data)
            .then(response => ({ 
                status: true, 
                number_status: response.status,
                data: response.data 
            }))
            .catch(error => ({
                status: false,
                number_status: error.response ? error.response.status : error, 
                data: error.response ? error.response.data : error.message 
            }))
        },
    
        async patch(url,data)
        {
            return await axios_instance.patch(url,data)
            .then(response => ({
                status: true,
                number_status: response.status,
                data: response.data
            }))
            .catch(error => ({
                status: false,
                number_status: error.response ? error.response.status : error,
                data: error.response ? error.response.data : error.message
            }))
        },
    
        async delete(url)
        {
            return await axios_instance.delete(url)
            .then(response => ({ 
                status: true, 
                number_status: response.status,
                data: response.data 
            }))
            .catch(error => ({
                status: false,
                number_status: error.response ? error.response.status : error, 
                data: error.response ? error.response.data : error.message 
            }))
        },

    },

}

export default connections