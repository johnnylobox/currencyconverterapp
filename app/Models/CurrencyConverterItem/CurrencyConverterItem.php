<?php

namespace App\Models\CurrencyConverterItem;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencyConverterItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'ip_address',
        'import',
        'iso_from',
        'iso_to',
        'rate_from',
        'rate_to',
        'convertion',
    ];

}
