<?php

namespace App\Http\Controllers\CurrencyConverterItem;

use App\Http\Controllers\Controller;
use App\Http\Requests\CurrencyConverterItem\CurrencyConverterItemRequest;
use App\Http\Resources\CurrencyConverterItem\CurrencyConverterItemResource;
use App\Models\CurrencyConverterItem\CurrencyConverterItem;
use Carbon\Carbon;

class CurrencyConverterItemController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(CurrencyConverterItemRequest $request)
    {
        $attemps = CurrencyConverterItem::where('ip_address', $request->ip_address)->whereDate('created_at', Carbon::now()->toDateString())->count();

        if($attemps >= 5)
        {
            return response()->json([
                'message' => 'Ha superado la cantidad de peticiones diarias',
            ], 429);
        }
        
        $new = CurrencyConverterItem::create($request->validated());
        return response()->json([
            'item' => new CurrencyConverterItemResource($new->fresh()),
        ]);
    }

    public function show(string $id)
    {
        //
    }

    public function edit(string $id)
    {
        //
    }

    public function update(CurrencyConverterItemRequest $request, string $id)
    {
        //
    }

    public function destroy(string $id)
    {
        //
    }
}
