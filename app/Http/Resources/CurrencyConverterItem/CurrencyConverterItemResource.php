<?php

namespace App\Http\Resources\CurrencyConverterItem;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyConverterItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'         => $this->id,
            'ip_address' => $this->ip_address,
            'import'     => $this->import,
            'iso_from'   => $this->iso_from,
            'iso_to'     => $this->iso_to,
            'rate_from'  => $this->rate_from,
            'rate_to'    => $this->rate_to,
            'convertion' => $this->convertion,
        ];
    }
}
