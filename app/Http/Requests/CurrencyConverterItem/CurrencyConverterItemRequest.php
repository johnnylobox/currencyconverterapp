<?php

namespace App\Http\Requests\CurrencyConverterItem;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyConverterItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'ip_address' => ['required', 'ip'],
            'import'     => ['required', 'numeric'],
            'iso_from'   => ['required', 'max:10'],
            'iso_to'     => ['required', 'max:10'],
            'rate_from'  => ['numeric'],
            'rate_to'    => ['numeric'],
            'convertion' => ['numeric'],
        ];
    }
}
